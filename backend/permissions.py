from nm2.lib.permissions import Permissions, Permission
from . import const


class PersonVisitorPermissions(Permissions):
    """
    Store NM-specific permissions
    """
    __slots__ = ("visitor", "person")

    edit_email = Permission(doc="the visitor can edit the person's preferred email address")
    edit_bio = Permission(doc="the visitor can edit the person's biography")
    update_keycheck = Permission(doc="the visitor can update keycheck results for this person")
    view_person_audit_log = Permission(doc="the visitor can view the audit logs for this person's data")
    request_new_status = Permission(doc="the visitor can request a new status for this person")
    edit_ldap = Permission(doc="the visitor can edit the person's LDAP-synced fields")
    edit_fpr = Permission(doc="the visitor can edit the person's key fingerprint")
    fd_comments = Permission(doc="the visitor can view the person's FD comments")

    def __init__(self, person, visitor, **kw):
        super(PersonVisitorPermissions, self).__init__(**kw)
        # Person being visited
        self.visitor = visitor
        self.person = person.person

        if self.visitor is None:
            pass
        elif self.visitor.is_admin:
            self._compute_admin_perms()
        elif self.visitor == self.person:
            self._compute_own_perms()
        elif self.visitor.is_am:
            self._compute_active_am_perms()
        elif self.visitor.is_dd:
            self._compute_dd_perms()

    def _person_has_ldap_record(self):
        """
        If the person is already in LDAP, then nobody can edit their LDAP
        info, since this database then becomes a read-only mirror of LDAP
        """
        return self.person.status not in (const.STATUS_DC, const.STATUS_DM)

    def _person_has_frozen_processes(self):
        """
        Return True if there are active processes currently frozen for review
        """
        import process.models as pmodels
        return pmodels.Process.objects.filter(
                person=self.person, frozen_by__isnull=False, closed_time__isnull=True).exists()

    def _compute_admin_perms(self):
        self.edit_email = True
        self.edit_bio = True
        self.update_keycheck = True
        self.view_person_audit_log = True
        if self.person.possible_new_statuses:
            self.request_new_status = True
        if not self._person_has_ldap_record():
            self.edit_ldap = True
            self.edit_fpr = True
        elif self.person.status in (const.STATUS_EMERITUS_DD, const.STATUS_REMOVED_DD):
            self.edit_fpr = True
        self.fd_comments = True

    def _compute_own_perms(self):
        self.edit_email = True
        self.update_keycheck = True
        if not self._person_has_frozen_processes():
            if not self.person.pending:
                if not self._person_has_ldap_record():
                    self.edit_ldap = True
                    self.edit_fpr = True
                elif self.person.status in (const.STATUS_EMERITUS_DD, const.STATUS_REMOVED_DD):
                    self.edit_fpr = True
            self.edit_bio = True
        if self.person.pending:
            return
        self.view_person_audit_log = True
        if self.person.possible_new_statuses:
            self.request_new_status = True

    def _compute_active_am_perms(self):
        self.update_keycheck = True
        self.view_person_audit_log = True
        if not self._person_has_frozen_processes():
            self.edit_bio = True
            if not self._person_has_ldap_record():
                self.edit_ldap = True
                self.edit_fpr = True
            elif self.person.status in (const.STATUS_EMERITUS_DD, const.STATUS_REMOVED_DD):
                self.edit_fpr = True

    def _compute_dd_perms(self):
        self.update_keycheck = True
        self.view_person_audit_log = True

    # TODO: advocate view audit log


class LegacyProcessVisitorPermissions(PersonVisitorPermissions):
    """
    Permissions for visiting old-style Processes
    """
    __slots__ = ("process",)

    view_mbox = Permission(doc="the visitor can view the mailbox of this process")

    def __init__(self, process, visitor):
        super().__init__(process.person, visitor)
        self.process = process

        if self.visitor is None:
            pass
        elif self.visitor.is_admin:
            self.view_mbox = True
        elif self.visitor == self.person:
            self.view_mbox = True
        elif self.visitor.is_am:
            self.view_mbox = True
        elif self.process.advocates.filter(pk=self.visitor.pk).exists():
            self.view_mbox = True
