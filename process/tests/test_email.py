from django.test import TestCase
from backend.models import Person
from dsa.models import LDAPFields
import datetime


def parse_headers(bytemsg):
    headers = []
    body = []
    in_body = False
    for line in bytemsg.splitlines():
        if in_body:
            body.append(line)
            continue
        if not line:
            in_body = True
            continue
        if line[0] in (ord(b' '), ord(b'\t')):
            headers[-1] += line
        else:
            headers.append(line)
    return dict(x.split(b": ", 1) for x in headers), body


class TestEmail(TestCase):
    def setUp(self):
        self.person = Person.objects.create_user(fullname="Ondřej Nový", email="ondrej@example.org", audit_skip=True)
        LDAPFields.objects.create(
                person=self.person, cn="Ondřej", sn="Nový", email="ondrej@example.org", audit_skip=True)
        self.addrstr = "test@example.org"
        self.addrtuple = ("Enric♥ Zìní", "enrico@example.org")
        self.date = datetime.datetime(2017, 6, 5, 4, 3, 2)
        self.args = {
            "from_email": self.person,
            "to": self.addrstr,
            "cc": [self.person, self.addrstr, self.addrtuple],
            "subject": "♥ Debian ♥",
            "date": self.date,
            "body": "Debian ♥ Debian",
        }

    def test_python(self):
        from process.email import build_python_message
        msg = build_python_message(**self.args)
        headers, body = parse_headers(msg.as_bytes())
        self.maxDiff = None
        self.assertEqual(headers, {
            b'Cc': b'=?utf-8?b?T25kxZllaiBOb3bDvQ==?= <ondrej@example.org>, test@example.org, =?utf-8?b?RW5yaWPimaUgWsOsbsOt?= <enrico@example.org>',
            b'Content-Transfer-Encoding': b'base64',
            b'Content-Type': b'text/plain; charset="utf-8"',
            b'Date': b'Mon, 05 Jun 2017 04:03:02 -0000',
            b'From': b'=?utf-8?b?T25kxZllaiBOb3bDvQ==?= <ondrej@example.org>',
            b'MIME-Version': b'1.0',
            b'Subject': b'=?utf-8?b?4pmlIERlYmlhbiDimaU=?=',
            b'To': b'test@example.org',
        })
        self.assertEqual(body, [b'RGViaWFuIOKZpSBEZWJpYW4='])

    def test_django(self):
        from process.email import build_django_message
        msg = build_django_message(**self.args)
        headers, body = parse_headers(msg.message().as_bytes())
        self.maxDiff = None

        # Django 1.11 no longer base64-encodes everything (#27333 upstream)
        if headers.get(b'Content-Transfer-Encoding') == b'base64':
            self.assertEqual(headers, {
                b'Cc': b'=?utf-8?b?PT91dGYtOD9iP1QyNWt4WmxsYWlCT2IzYkR2UT09Pz0=?= <ondrej@example.org>, test@example.org, =?utf-8?b?RW5yaWPimaU=?=, enrico@example.org',
                b'Content-Transfer-Encoding': b'base64',
                b'Content-Type': b'text/plain; charset="utf-8"',
                b'Date': b'Mon, 05 Jun 2017 04:03:02 -0000',
                b'From': b'=?utf-8?b?T25kxZllaiBOb3bDvQ==?= <ondrej@example.org>',
                b'MIME-Version': b'1.0',
                b'Subject': b'=?utf-8?b?4pmlIERlYmlhbiDimaU=?=',
                b'To': b'test@example.org',
            })
            self.assertEqual(body, [b'RGViaWFuIOKZpSBEZWJpYW4='])
        else:
            self.assertIsNotNone(headers.pop(b"Message-ID"))
            self.assertEqual(headers, {
                b'Cc': b'=?utf-8?b?PT91dGYtOD9iP1QyNWt4WmxsYWlCT2IzYkR2UT09Pz0=?= <ondrej@example.org>, test@example.org, =?utf-8?b?RW5yaWPimaU=?=, enrico@example.org',
                b'Content-Transfer-Encoding': b'8bit',
                b'Content-Type': b'text/plain; charset="utf-8"',
                b'Date': b'Mon, 05 Jun 2017 04:03:02 -0000',
                b'From': b'=?utf-8?b?T25kxZllaiBOb3bDvQ==?= <ondrej@example.org>',
                b'MIME-Version': b'1.0',
                b'Subject': b'=?utf-8?b?4pmlIERlYmlhbiDimaU=?=',
                b'To': b'test@example.org',
            })
            self.assertEqual(body, ['Debian ♥ Debian'.encode("utf8")])
