{% load nm %}
{% autoescape off %}
Hi

{{request}}
  Key fingerprint:  {{person.fpr}}
  uid:              {{person.ldap_fields.uid}}
  First name:       {{person.ldap_fields.cn}}
  Middle name:      {{person.ldap_fields.mn|default:"-"}}
  Last name:        {{person.ldap_fields.sn}}
  Current status:   {{person|desc_status}}
  Target keyring:   {{process.applying_for|desc_status}}
  Forward email:    {{person.ldap_fields.email}}
  Details:          {{process_url}}
  {% if person.status == "dm" and process.applying_for == "dm_ga" or process.applying_for == "dc_ga" %}Access period:    default
  Host(s)/Arch(s):  FIXME                   <<<<-------------------{% endif %}

{% if not retiring %}{{person.fullname}} has accepted the DMUP in a signed statement.{% endif %}

{{intents}}

-- 
Thank you,
{% with "dc_ga dm dm_ga dd_e" as list %}
{% if process.applying_for in list %}
{{visitor.fullname}} (as Front Desk)
{% else %}
{{visitor.fullname}} (as DAM)
{% endif %}
{% endwith %}
{% endautoescape %}
