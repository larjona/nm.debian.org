from django.contrib import admin
from legacy.models import Process, Log


class LogInline(admin.TabularInline):
    model = Log


class ProcessAdmin(admin.ModelAdmin):
    raw_id_fields = ('manager',)
    filter_horizontal = ("advocates",)
    search_fields = ("person__cn", "person__sn", "person__email", "person__uid")


admin.site.register(Process, ProcessAdmin)


class LogAdmin(admin.ModelAdmin):
    raw_id_fields = ('changed_by',)


admin.site.register(Log, LogAdmin)
