"""
Test DM claim interface
"""
from __future__ import annotations
from typing import Tuple
from django.test import TestCase
from django.urls import reverse
from backend.models import Person, Fingerprint
from backend.unittest import PersonFixtureMixin
from signon.models import Identity


class TestClaim(PersonFixtureMixin, TestCase):
    # Use an old, not yet revoked key of mine
    test_fingerprint = "66B4DFB68CB24EBBD8650BC4F4B4B0CC797EBFAB"

    @classmethod
    def __add_extra_tests__(cls):
        for person in ("pending", "dc", "dc_ga", "dm", "dm_ga", "dd_e", "dd_r"):
            cls._add_method(cls._test_success, person)

        for person in ("dd_nu", "dd_u", "fd", "dam"):
            cls._add_method(cls._test_is_dd, person)

    def make_test_client(self, person):
        """
        Override the default make_test_client to allow sso-logged-in people
        with no corresponding Person record in the database
        """
        if person and "@" in person:
            return super(TestClaim, self).make_test_client(None, sso_username=person)
        else:
            return super(TestClaim, self).make_test_client(person)

    def get_confirm_url(self, person) -> Tuple[str, str]:
        """
        Set up a test case where person has an invalid username and
        self.test_fingerprint as fingerprint, and request a claim url.

        Returns the original username of the person, and the plaintext claim
        url.
        """
        # Unbind the current identity
        person = self.persons[person]
        identity = person.identities.get(issuer="debsso")
        identity.person = None
        identity.save(audit_skip=True)
        person.save(audit_skip=True)

        # Make sure the person has a key associated
        Fingerprint.objects.create(fpr=self.test_fingerprint, person=person, is_active=True, audit_skip=True)

        # Create an unbound
        client = self.make_test_client(identity.subject)
        response = client.get(reverse("dm_claim"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["identity"], identity)

        response = client.post(reverse("dm_claim"), data={"fpr": self.test_fingerprint})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["identity"], identity)
        self.assertEqual(response.context["fpr"].fpr, self.test_fingerprint)
        self.assertIn("/dm/claim/confirm", response.context["plaintext"])
        self.assertIn(b"-----BEGIN PGP MESSAGE-----", response.context["challenge"])
        return identity.subject, response.context["plaintext"].strip()

    def _test_success(self, person):
        orig_username, confirm_url = self.get_confirm_url(person)
        client = self.make_test_client(orig_username)
        response = client.get(confirm_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["errors"], [])
        # The identity is now bound has now been set
        identity = Identity.objects.get(issuer="debsso", subject=orig_username)
        self.assertEqual(identity.person, self.persons[person])

    def _test_is_dd(self, person):
        orig_username, confirm_url = self.get_confirm_url(person)
        client = self.make_test_client(orig_username)
        response = client.get(confirm_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["errors"], ["The GPG fingerprint corresponds to a Debian Developer."])
        person = Person.objects.get(pk=self.persons[person].pk)
        # The identity is still unbound
        identity = Identity.objects.get(issuer="debsso", subject=orig_username)
        self.assertIsNone(identity.person)

    def test_anonymous(self):
        client = self.make_test_client(None)
        response = client.get(reverse("dm_claim"))
        self.assertPermissionDenied(response)
        response = client.get(reverse("dm_claim_confirm", kwargs={"token": "123456"}))
        self.assertPermissionDenied(response)
