from __future__ import annotations
from django.test import TestCase
from backend.unittest import BaseFixtureMixin
from backend import const
from signon.models import Identity
import json


class TestIdentity(BaseFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # Create a test person
        cls.persons.create("dd", status=const.STATUS_DD_NU)

    def test_create(self):
        # Create an unbound identity
        identity = Identity.objects.create(
                issuer="debsso", subject="dd@debian.org",
                audit_author=self.persons.dd, audit_notes="created test identity")

        self.assertIsNone(identity.person)

        log = list(identity.audit_log.all())
        self.assertEqual(len(log), 1)
        log = log[0]
        self.assertEqual(log.identity, identity)
        self.assertEqual(log.author, self.persons.dd)
        self.assertEqual(log.notes, "created test identity")
        self.assertEqual(json.loads(log.changes), {
            'fullname': [None, ''],
            'issuer': [None, 'debsso'],
            'person': [None, None],
            'picture': [None, ''],
            'profile': [None, ''],
            'subject': [None, 'dd@debian.org'],
            'username': [None, ''],
        })

        self.assertEqual(str(identity), "-:debsso:dd@debian.org")

        # Bind it
        identity.person = self.persons.dd
        identity.save(audit_author=self.persons.dd, audit_notes="Bound to person")

        log = identity.audit_log.order_by("-logdate", "-id").first()
        self.assertEqual(log.identity, identity)
        self.assertEqual(log.author, self.persons.dd)
        self.assertEqual(log.notes, "Bound to person")
        self.assertEqual(json.loads(log.changes), {
            'person': [None, "dd"],
        })

        self.assertEqual(str(identity), "dd:debsso:dd@debian.org")
